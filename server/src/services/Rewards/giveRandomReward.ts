import config from '../../config'

import {
  NO_MATCHING_ASSETS,
  REWARD_ISSUE_FAILURE,
} from '../../errors'

import { 
  rewardPriceRange as rewardPriceRangeType,
} from '../types'

import {
  Asset,
  Broker,
  MatchingRewardAsset
} from '../interfaces'

import getRewardTier from './getRewardTier'
import getRandomAssetInPriceRange from './getRandomAssetInPriceRange'

const giveRandomReward = (broker: Broker) => async (toAccount: string): Promise<Asset> => {
  // get reward tier
  const rewardTier: string = getRewardTier()
  const rewardPriceRange: rewardPriceRangeType = config.rewards[rewardTier]
  
  const matchingAsset: MatchingRewardAsset = await getRandomAssetInPriceRange(broker)(rewardPriceRange, config.rewards.sharesPerReward)
  
  if (!matchingAsset.tickerSymbol) {
    throw NO_MATCHING_ASSETS
  }

  if (matchingAsset.status === 'buy more') {
    await broker.buySharesInRewardsAccount(matchingAsset.tickerSymbol, config.rewards.sharesPerReward)
  }

  // issue reward
  const { success } = await broker.moveSharesFromRewardsAccount(toAccount, matchingAsset.tickerSymbol, config.rewards.sharesPerReward)

  if (success) {
    return {
      tickerSymbol: matchingAsset.tickerSymbol,
      quantity: config.rewards.sharesPerReward
    }
  }
  else {
    throw REWARD_ISSUE_FAILURE
  }
}

export default giveRandomReward