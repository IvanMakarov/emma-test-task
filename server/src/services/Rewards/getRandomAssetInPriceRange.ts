import shuffle from '../../utils/shuffle'

import {
  SERVICE_NOT_AVAILABLE,
} from '../../errors'

import { 
  rewardPriceRange as rewardPriceRangeType,
} from '../types'

import {
  Broker,
  MatchingRewardAsset
} from '../interfaces'

import isPriceInRange from './isPriceInRange'
import isAssetInMatchingPriceBracket from './isAssetInMatchingPriceBracket'

const getRandomAssetInPriceRange = (broker: Broker) => async (rewardPriceRange: rewardPriceRangeType, rewardsCount: number) => {
  const matchingAsset: MatchingRewardAsset = {
    tickerSymbol: null,
    status: null
  }

  // select a matching asset from existing rewards
  const existingRewards = await broker.getRewardsAccountPositions()

  const doesPriceMatch = isPriceInRange(rewardPriceRange)

  for (const { tickerSymbol, pricePerShare, quantity } of shuffle(existingRewards)) {
    if (doesPriceMatch(pricePerShare) && quantity >= rewardsCount) {
      matchingAsset.tickerSymbol = tickerSymbol
      matchingAsset.status = 'in rewards'
      break
    }
  }

  // if no matching shares in the rewards account, we need to find more to purchase
  if (!matchingAsset.tickerSymbol) {
    const marketStatus = await broker.isMarketOpen()

    if (!marketStatus.open) {
      throw SERVICE_NOT_AVAILABLE
    }

    const tradableAssets = await broker.listTradableAssets()

    const doesAssetMatch = isAssetInMatchingPriceBracket(broker.getLatestPrice)(rewardPriceRange)
    
    for (const { tickerSymbol } of shuffle(tradableAssets)) {
      const isTheRightPrice = await doesAssetMatch(tickerSymbol)

      if (isTheRightPrice) {
        matchingAsset.tickerSymbol = tickerSymbol
        matchingAsset.status = 'buy more'
        break
      }
    }
  }

  return matchingAsset
}

export default getRandomAssetInPriceRange