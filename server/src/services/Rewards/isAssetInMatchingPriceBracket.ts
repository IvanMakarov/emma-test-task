import {
  tickerSymbol as tickerSymbolType,
  rewardPriceRange as rewardPriceRangeType
} from '../types'

import {
  getLatestPriceFn
} from '../interfaces'

import isPriceInRange from "./isPriceInRange"

const isAssetInMatchingPriceBracket = (getLatestPrice: getLatestPriceFn) => (priceRange: rewardPriceRangeType) => async (tickerSymbol: tickerSymbolType): Promise<boolean> => {
  const { sharePrice } = await getLatestPrice(tickerSymbol)

  const result = isPriceInRange(priceRange)(sharePrice) 

  return result
}

export default isAssetInMatchingPriceBracket