import config from '../../config'
import probabilitize from '../../utils/probabilitize'

const outcomes = [
  {
    value: 'expensive',
    probability: config.rewards.chanceExpensive,
  },
  {
    value: 'medium',
    probability: config.rewards.chanceMedium,
  },
  {
    value: 'cheap',
    probability: config.rewards.chanceCheap,
  },
]

const getRewardTier = probabilitize(outcomes)

export default getRewardTier
