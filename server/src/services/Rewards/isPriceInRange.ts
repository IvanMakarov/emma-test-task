const isPriceInRange = (range: [number, number]) => (price: number) => {
  return (price > range[0]) && (price <= range[1])
}

export default isPriceInRange