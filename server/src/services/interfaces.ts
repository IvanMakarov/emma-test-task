import {
  tickerSymbol,
  rewardsAssetStatus
} from './types'

export interface AssetPrice {
  sharePrice
}

export interface Asset {
  tickerSymbol: tickerSymbol 
  quantity: number
}
export interface Success {
  success: boolean
}
export interface MarketHours {
  open: boolean,
  nextOpeningTime: string, 
  nextClosingTime: string
}

export interface TradableAsset {
  tickerSymbol: tickerSymbol;
}

export interface SharePurchaseResult extends Success {
  sharePricePaid: number;
}

export interface RewardAccountItem extends Asset {
  pricePerShare: number;
}

export interface getLatestPriceFn {
  (tickerSymbol: tickerSymbol): Promise<AssetPrice>
}

export interface moveSharesFromRewardsAccountFn {
  (toAccount: string, tickerSymbol: tickerSymbol, quantity: number): Promise<Success>
}


export interface listTradableAssetsFn {
  (): Promise<Array<TradableAsset>>
} 

export interface isMarketOpenFn {
  (): Promise<MarketHours>
}

export interface buySharesInRewardsAccountFn {
  (tickerSymbol: tickerSymbol, quantity: number): Promise<SharePurchaseResult>
}

export interface getRewardsAccountPositionsFn {
  (): Promise<Array<RewardAccountItem>>
}

export interface MatchingRewardAsset extends TradableAsset {
  status: rewardsAssetStatus
}

export interface Broker {
  moveSharesFromRewardsAccount: moveSharesFromRewardsAccountFn
  getLatestPrice: getLatestPriceFn,
  listTradableAssets: listTradableAssetsFn,
  isMarketOpen: isMarketOpenFn,
  buySharesInRewardsAccount: buySharesInRewardsAccountFn,
  getRewardsAccountPositions: getRewardsAccountPositionsFn,
}