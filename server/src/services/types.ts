export type tickerSymbol = string
export type sharePrice = number
export type rewardPriceRange = [sharePrice, sharePrice]
export type rewardsAssetStatus = 'in rewards' | 'buy more'
