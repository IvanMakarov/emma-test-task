import datetime from '../../utils/datetime'

export type workingDays = Array<number>
export type workTime = string // 'HH:mm'

export const getDaysBetween = (day: number, nextDay: number): number => {
  if (nextDay >= day) return nextDay - day

  return nextDay + 7 - day
}

class Schedule {
  private workingDays: workingDays
  private openTime: Array<number>;
  private closeTime: Array<number>;

  constructor (
    // I assume a regular business day schedule, which should be sufficient for the purposes of this assignment
    // 0 == Sunday, 6 == Saturday; must be in order
    workingDays: workingDays = [1, 2, 3, 4, 5],
    openTime: workTime = '09:00',
    closeTime: workTime = '19:00'
  ) {
    this.workingDays = workingDays
    this.openTime = openTime.split(':').map(item => +item)
    this.closeTime = closeTime.split(':').map(item => +item)
  }

  private isNowWorkingDay (now: datetime.Dayjs): boolean {
    return this.workingDays.includes(now.day())
  }

  private isBeforeOpenTime (now: datetime.Dayjs): boolean {
    const openTime = now.clone().hour(this.openTime[0]).minute(this.openTime[1])

    return now.isBefore(openTime)
  }

  private isNowWorkingHours (now: datetime.Dayjs): boolean {
    const openTime = now.clone().hour(this.openTime[0]).minute(this.openTime[1])
    const closeTime = now.clone().hour(this.closeTime[0]).minute(this.closeTime[1])

    return now.isBetween(openTime, closeTime)
  }

  private getNextWorkingDay (today: number): number {
    const nextDay = this.workingDays.find(day => day > today)

    return nextDay || this.workingDays[0]
  }

  private nextWorkingDay (now: datetime.Dayjs): datetime.Dayjs {
    if (this.isNowWorkingDay(now) && this.isBeforeOpenTime(now)) return now

    const day = now.day()

    const nextWorkingDay = this.getNextWorkingDay(day)

    const daysDiff = getDaysBetween(day, nextWorkingDay)

    return now
      .clone()
      .add(daysDiff, 'days')
  }
  
  isOpen (now: datetime.Dayjs): boolean {
    return this.isNowWorkingDay(now) && this.isNowWorkingHours(now)
  }

  nextOpeningTime (now: datetime.Dayjs): string {
    return this.nextWorkingDay(now)
      .hour(+this.openTime[0])
      .minute(+this.openTime[1])
      .format('YYYY-MM-DD HH:mm')
  }

  nextClosingTime (now: datetime.Dayjs):string {
    if (this.isOpen(now)) {
      return now.clone()
        .hour(this.closeTime[0])
        .minute(this.closeTime[1])
        .format('YYYY-MM-DD HH:mm')
    }

    return this.nextWorkingDay(now)
      .hour(+this.closeTime[0])
      .minute(+this.closeTime[1])
      .format('YYYY-MM-DD HH:mm')
  }
}

export default Schedule