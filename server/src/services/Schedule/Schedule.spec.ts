import chai from 'chai';

import datetime from '../../utils/datetime'

import Schedule, { getDaysBetween } from './index'

const expect = chai.expect

describe('getDaysBetween', () => {
  it('should return correct absolute difference between days of the week', () => {
    expect(getDaysBetween(1, 2)).to.equal(1)
    expect(getDaysBetween(1, 6)).to.equal(5)
    expect(getDaysBetween(1, 0)).to.equal(6)
    expect(getDaysBetween(5, 2)).to.equal(4)
  })
})

describe('Schedule', () => {
  const schedule = new Schedule(
    [1, 2, 3, 5],
    '10:00',
    '13:30',
  )
  const date1 = datetime('2022-02-22 17:00', 'YYYY-MM-DD HH:mm') // Tue
  const date2 = datetime('2022-02-23 09:00', 'YYYY-MM-DD HH:mm') // Wed
  const date3 = datetime('2022-02-23 10:01', 'YYYY-MM-DD HH:mm') // Wed
  const date4 = datetime('2022-02-23 13:31', 'YYYY-MM-DD HH:mm') // Wed
  const date5 = datetime('2022-02-24 12:00', 'YYYY-MM-DD HH:mm') // Thu
  const date6 = datetime('2022-02-25 12:00', 'YYYY-MM-DD HH:mm') // Fri

  it ('should return correct open status', () => {
    expect(schedule.isOpen(date1)).to.be.false
    expect(schedule.isOpen(date2)).to.be.false
    expect(schedule.isOpen(date3)).to.be.true
    expect(schedule.isOpen(date4)).to.be.false
    expect(schedule.isOpen(date5)).to.be.false
    expect(schedule.isOpen(date6)).to.be.true
  })

  it ('should return correct next opening time', () => {
    expect(schedule.nextOpeningTime(date1)).to.equal('2022-02-23 10:00')
    expect(schedule.nextOpeningTime(date2)).to.equal('2022-02-23 10:00')
    expect(schedule.nextOpeningTime(date3)).to.equal('2022-02-25 10:00')
    expect(schedule.nextOpeningTime(date4)).to.equal('2022-02-25 10:00')
    expect(schedule.nextOpeningTime(date5)).to.equal('2022-02-25 10:00')
    expect(schedule.nextOpeningTime(date6)).to.equal('2022-02-28 10:00')
  })

  it ('should return correct next closing time', () => {
    expect(schedule.nextClosingTime(date1)).to.equal('2022-02-23 13:30')
    expect(schedule.nextClosingTime(date2)).to.equal('2022-02-23 13:30')
    expect(schedule.nextClosingTime(date3)).to.equal('2022-02-23 13:30')
    expect(schedule.nextClosingTime(date4)).to.equal('2022-02-25 13:30')
    expect(schedule.nextClosingTime(date5)).to.equal('2022-02-25 13:30')
    expect(schedule.nextClosingTime(date6)).to.equal('2022-02-25 13:30')
  })
})