import random from '../../utils/random'
import datetime from '../../utils/datetime'

import config from '../../config'

import {
  SERVICE_NOT_AVAILABLE
} from '../../errors'

import {
  tickerSymbol as tickerSymbolType,
  sharePrice as sharePriceType,
} from '../types'

import {
  Broker as IBroker,

  AssetPrice,
  Success,
  MarketHours,
  TradableAsset,
  SharePurchaseResult,
  RewardAccountItem,
} from '../interfaces'

import Schedule from '../Schedule'

import {
  Rewards,
  User
} from '../../models'

import tickerSymbols from './tickerSymbols'

// assuming there is always a share in the required price range
const getRandomPrice = (): sharePriceType => random(config.rewards.cheap[0], config.rewards.expensive[1], true)

class Broker implements IBroker {
  private schedule: Schedule
  private assets: { [key: tickerSymbolType]: sharePriceType }

  constructor () {
    this.schedule = new Schedule(
      [1, 2, 3, 4, 5],
      '09:00',
      '19:00',
    )

    // this is to ensure the price is the same while a Broker instance is used
    // apparently it is going to be different in a real-life situation
    this.assets = tickerSymbols.reduce((acc, item) => {
      acc[item] = getRandomPrice()
      return acc
    }, {})
  }

  isMarketOpen (): Promise<MarketHours> {
    return new Promise((resolve, reject) => {
      const now = datetime()

      setTimeout(() => {
        resolve({
          open: this.schedule.isOpen(now),
          nextOpeningTime: this.schedule.nextOpeningTime(now),
          nextClosingTime: this.schedule.nextClosingTime(now)
        })
      }, 0)
    })
  }

  listTradableAssets(): Promise<Array<TradableAsset>> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const assetsList = Object.keys(this.assets).map(item => ({
          tickerSymbol: item
        }))

        resolve(assetsList)
      }, 0)
    })
  }

  getLatestPrice (tickerSymbol: tickerSymbolType): Promise<AssetPrice> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ sharePrice: this.assets[tickerSymbol] })
      }, 0)
    })
  }

  async buySharesInRewardsAccount (tickerSymbol: tickerSymbolType, quantity: number): Promise<SharePurchaseResult> {
    const marketStatus = await this.isMarketOpen()

    if (!marketStatus.open) {
      throw SERVICE_NOT_AVAILABLE
    }

    try {
      const assetLatestPrice = await this.getLatestPrice(tickerSymbol)
  
      const sharePricePaid = assetLatestPrice.sharePrice * quantity
  
      // update reward account
      const reward = await Rewards.findOne({
        tickerSymbol
      })

      if (reward) {
        await Rewards.findOneAndUpdate(
          { tickerSymbol },
          { $inc: { quantity: +quantity }}
        )
      }
      else {
        await Rewards.create(
          { 
            tickerSymbol, 
            quantity: +quantity
          },
        )
      }
      //
  
      return {
        success: true,
        sharePricePaid
      }
    }
    catch (error) {
      throw new Error("Can't buy shares")
    }
  }

  async getRewardsAccountPositions (): Promise<Array<RewardAccountItem>> {
    const availableRewardsList = await Rewards.find() // intentionally not implementing pagination here

    const addPriceRequests = availableRewardsList.map(async item => {
      const { sharePrice } = await this.getLatestPrice(item.tickerSymbol)

      return {
        tickerSymbol: item.tickerSymbol,
        quantity: item.quantity,
        pricePerShare: sharePrice
      }
    })

    return Promise.all(addPriceRequests)
  }

  async moveSharesFromRewardsAccount (toAccount: string, tickerSymbol: tickerSymbolType, quantity: number): Promise<Success> {
    try {
      await Rewards.findOneAndUpdate(
        { tickerSymbol },
        { $inc: { quantity: -quantity }}
      )

      const { assets } = await User
        .findById(toAccount)
        .select('assets')

      const existingAssetIndex = assets.findIndex(item => item.tickerSymbol === tickerSymbol)
            
      const assetItem = assets[existingAssetIndex]

      if (!assetItem) {
        assets.push({
          tickerSymbol, 
          quantity
        }) 
      }
      else {
        assets[existingAssetIndex] = {
          _id: assetItem._id,
          tickerSymbol: assetItem.tickerSymbol,
          quantity: +assetItem.quantity + quantity
        }
      }

      await User.findByIdAndUpdate(toAccount, {
        assets
      })

      return {
        success: true
      }
    }
    catch (error) {
      return {
        success: false,
      }
    }
  }
}

export default Broker