import { Request, Response, NextFunction } from 'express'

const setResponseHeaders = (req: Request, res: Response, next: NextFunction) => {
  res.header('Access-Control-Allow-Origin', req.get('origin'));
  res.header('Access-Control-Allow-Methods', 'DELETE, GET, OPTIONS, PATCH, POST, PUT');
  res.header('Access-Control-Max-Age', '86400');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cookie, External-Client'); // eslint-disable-line max-len
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Allow', 'DELETE, GET, OPTIONS, PATCH, POST, PUT');
  if (req.method === 'OPTIONS') {
    res.send(200);
    return null;
  }
  return next();
};

export default setResponseHeaders;
