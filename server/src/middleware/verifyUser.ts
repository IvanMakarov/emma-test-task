import { Request, Response, NextFunction } from 'express'

import decodeUserJwt from '../auth/decodeUserJwt'

export default (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization;

  if (!token) {
    res.status(401).send()
  }

  const validUser = decodeUserJwt(token)
  
  if (!validUser) {
    res.status(401).send()
  }
  else {
    req.user = validUser

    next()
  }
}