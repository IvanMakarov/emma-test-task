const rewards = {
  // Let's assume we are always going to have three reward tiers
  // Otherwise we could customize it via env variable in a more sophisticated way
  chanceExpensive: +process.env.REWARD_CHANCE_EXPENSIVE || 0.2,
  chanceMedium: +process.env.REWARD_CHANCE_MEDIUM || 0.3,
  chanceCheap: +process.env.REWARD_CHANCE_CHEAP || 0.5,
  expensive: [+process.env.REWARD_EXPENSIVE_MIN, +process.env.REWARD_EXPENSIVE_MAX],
  medium: [+process.env.REWARD_MEDIUM_MIN, +process.env.REWARD_MEDIUM_MAX],
  cheap: [+process.env.REWARD_CHEAP_MIN, +process.env.REWARD_CHEAP_MAX],
  sharesPerReward: 1,
}

export default {
  jwtSecret: process.env.JWT_SECRET || 'such_secret_much_mystery',
  rewards
}