import Probability from 'probability-node'

type value = any;

interface Outcome {
  value: value;
  probability: string | number;
}

const probabilitize = (outcomes: Array<Outcome>): value => {
  const probabilitized = new Probability(outcomes.map(item => ({
    f: () => item.value, // this is to avoid immediate execution of the function and enable passing other data types
    p: item.probability
  })))

  return probabilitized
}

export default probabilitize