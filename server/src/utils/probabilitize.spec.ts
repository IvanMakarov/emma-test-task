import chai from 'chai';

import probabilitize from './probabilitize'

const expect = chai.expect

const OUTCOMES = [
  'high',
  'medium',
  'low',
]

const PROBABILITIES = [
  {
    value: OUTCOMES[0], 
    probability: 0.95
  },
  {
    value: OUTCOMES[1], 
    probability: 0.03
  },
  {
    value: OUTCOMES[2], 
    probability: 0.02
  },
]

const PROBABILITY_EXPERIMENT_COUNT = 1 * 1e7
const ACCEPTABLE_DEVIATION_FRACTION = 1/10

const probabilitized = probabilitize(PROBABILITIES)

describe('probabilitize', () => {
  it('should resolve with a preset value', () => {
    expect(OUTCOMES).to.include(probabilitized())
  })

  it('should resolve at the set probability', () => {
    const actualOutcomes = {}

    for (let i = 0; i < PROBABILITY_EXPERIMENT_COUNT; i += 1) {
      const outcome = probabilitized()

      actualOutcomes[outcome]
        ? actualOutcomes[outcome] += 1
        : actualOutcomes[outcome] = 1
    }

    PROBABILITIES.forEach((item) => {
      const expectedOutcomes = PROBABILITY_EXPERIMENT_COUNT * item.probability
      const acceptableDeviation = expectedOutcomes * ACCEPTABLE_DEVIATION_FRACTION

      expect(actualOutcomes[item.value]).to.be.within(
        expectedOutcomes - acceptableDeviation,
        expectedOutcomes + acceptableDeviation,
      )
    })
  })
})
