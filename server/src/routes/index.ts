import express from 'express'
import asyncHandler from 'express-async-handler'

import verifyUser from '../middleware/verifyUser'

import users from '../controllers/users';
import rewards from '../controllers/rewards';

const router = express.Router();

router
  .route('/register')
  .post(
    asyncHandler(users.create),
  );

router
  .route('/login')
  .post(
    asyncHandler(users.login),
  );

router
  .route('/claim-free-share')
  .post(
    verifyUser,
    asyncHandler(rewards.give),
  );

export default router
