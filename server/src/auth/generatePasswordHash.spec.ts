import chai from 'chai';

import generatePasswordHash from './generatePasswordHash';

const expect = chai.expect

describe('generatePasswordHash', () => {
  it('should return a different string', () => {
    const password = "password"

    const hash = generatePasswordHash(password)

    expect(hash).to.be.a('string')
    expect(hash).not.to.be.equal(password)
    // actually testing the hash function is out of scope of this project
  })
})