import tokenize from "./tokenize"

export default (user: {login: string, id: string|number}): string => {
  const userData = {
    login: user.login,
    id: user.id
  }

  return tokenize(userData)
}