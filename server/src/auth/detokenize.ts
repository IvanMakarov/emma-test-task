import jwt from 'jsonwebtoken';

import config from '../config'

interface IDetokenized {
  iat: number;
  exp: number;
  [key: string]: any;
}

export default (token: string): IDetokenized => {
  if (jwt.verify(token, config.jwtSecret)) {
    return jwt.decode(token);
  }

  return null
} 