import detokenize from './detokenize'

interface User {
  login: string, 
  id: number
}

export default (token: string): User => {
  const user = detokenize(token)

  if (user) {
    return {
      login: user.login,
      id: user.id,
    }
  }
  
  return null
} 