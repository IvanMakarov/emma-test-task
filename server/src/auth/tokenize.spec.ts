import chai from 'chai';

import tokenize from './tokenize';

const expect = chai.expect

describe('tokenize', () => {
  it('should return a string', () => {
    const payload = {
      login: 'foo',
      id: 1,
    }

    const token = tokenize(payload)

    expect(token).to.be.a('string')
  })
})