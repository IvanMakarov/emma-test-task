import jwt from 'jsonwebtoken';

import config from '../config'

const tokenize = (payload: object, expiresIn = '7d'): string => {
  return jwt.sign(payload, config.jwtSecret, { expiresIn });
}

export default tokenize