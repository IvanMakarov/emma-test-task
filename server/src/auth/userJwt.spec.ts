import chai from 'chai';

import buildUserJwt from './buildUserJwt';
import decodeUserJwt from './decodeUserJwt';

const expect = chai.expect

describe('userJwt', () => {
  it('should build and decode correctly', () => {
    const payload = {
      login: 'foo',
      id: 1,
    }

    const token = buildUserJwt(payload)
    const detokenized = decodeUserJwt(token)

    expect(payload).to.be.deep.equal(detokenized)
  })
})