import chai from 'chai';

import generatePasswordHash from './generatePasswordHash';
import comparePasswordToHash from './comparePasswordToHash';

const expect = chai.expect

describe('password check', () => {
  const password1 = 'foobar'
  const password2 = 'fizzbuzz'

  const hash = generatePasswordHash(password1)

  it('should compare passwords correctly', () => {
    const validResult = comparePasswordToHash(password1, hash)
    const invalidResult = comparePasswordToHash(password2, hash)

    expect(validResult).to.be.true
    expect(invalidResult).to.be.false
  })
})