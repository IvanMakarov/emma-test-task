import chai from 'chai';

import tokenize from './tokenize';
import detokenize from './detokenize';

const expect = chai.expect

describe('detokenize', () => {
  const payload = {
    login: 'foo',
    id: 1
  }

  const token = tokenize(payload)  
  const decodedToken = detokenize(token)

  it('should decode a token correctly', () => {
    expect(payload.login).to.be.equal(decodedToken.login)
    expect(payload.id).to.be.equal(decodedToken.id)
  })

  it('should contain expiration time', () => {
    expect(decodedToken.exp).to.be.a('number')
  })

  it('should contain issued at time', () => {
    expect(decodedToken.iat).to.be.a('number')
  })
})
