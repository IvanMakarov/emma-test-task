import crypto from 'crypto'

// md5 is used for the purposes of this test assignment only
// this algorithm is inherently flawed and should not be used in production as authentication means
// consider using alternatives such as bcrypt
const generatePasswordHash = (password: string): string => crypto.createHash('md5').update(password).digest('hex')

export default generatePasswordHash