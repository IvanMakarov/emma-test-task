import mongoose from 'mongoose'

export interface IReward {
  tickerSymbol: string; 
  quantity: number;
}

const Rewards = new mongoose.Schema<IReward>({
  tickerSymbol: { type: String },
  quantity: { type: Number },
})

export default mongoose.model('rewards', Rewards)