import mongoose from 'mongoose'

export interface IAsset {
  tickerSymbol: string; 
  quantity: number;
}

export interface IUser {
  login: string,
  passwordHash: string,
  registerDate: Date,
  assets?: Array<IAsset>,
  isEligibleToReward: boolean,
}

const User = new mongoose.Schema<IUser>({
  login: { type: String },
  passwordHash: { type: String },
  registerDate: { type: Date },
  assets: {
    type: [{
      tickerSymbol: { type: String },
      quantity: { type: Number },
    }],
    default: []
  },
  isEligibleToReward: {
    type: Boolean,
    default: false,
  }
})

export default mongoose.model('users', User)