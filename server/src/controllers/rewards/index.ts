import express from 'express';

import {
  AvailabilityError,
  NotFoundError,
} from '../../errors'

import User from '../../models/User'

import { giveRandomReward } from '../../services/Rewards'
import Broker from '../../services/Broker'

export default {
  give: async (req: express.Request, res: express.Response) => {
    const userId = `${req.user.id}`
    
    try {
      // We are checking for eligibility but its update implementation is out of scope of this project
      const { isEligibleToReward } = await User.findById(userId).select('isEligibleToReward')

      if (!isEligibleToReward) {
        res.status(403).send()
        return
      }
    } catch (error) {
      res.sendStatus(500)
    }

    try {
      const broker = new Broker
      const reward = await giveRandomReward(broker)(userId)
      res.status(200).send({ reward })
    }
    catch (error) {
      console.log(error) 
      
      if (error instanceof AvailabilityError) {
        res.sendStatus(503)
        return
      }

      if (error instanceof NotFoundError) {
        res.sendStatus(404)
        return
      }
      
      res.sendStatus(500)
    }
  },
}

