import express from 'express';
import bodyParser from 'body-parser';

import mongoose from 'mongoose'

import setResponseHeaders from './middleware/setResponseHeaders';

import routes from './routes'

declare global {
  namespace Express { // eslint-disable-line @typescript-eslint/no-namespace
    interface Request {
      user?: {
        login: string,
        id: number
      }
    }
  }
}

const app = express();
const port = 3000;

app.use([
  bodyParser.json(),
  setResponseHeaders
]);

app.get('/health', (req, res) => {
  res.status(200).send("It's alive!");
});

app.use('/v1', routes) 

mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}, () => {
  console.log('connected to database')
})

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});