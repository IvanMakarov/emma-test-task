export class AvailabilityError extends Error {
  constructor(message) {
    super(message);
    this.name = "AvailabilityError";
  }
}
export class NotFoundError extends Error {
  constructor(message) {
    super(message);
    this.name = "NotFoundError";
  }
}

export class ServiceError extends Error {
  constructor(message) {
    super(message);
    this.name = "ServiceError";
  }
}

export const SERVICE_NOT_AVAILABLE = new AvailabilityError('Service not available')
export const NO_MATCHING_ASSETS = new NotFoundError('No matching assets')
export const REWARD_ISSUE_FAILURE = new NotFoundError('Failed to issue a reward')
