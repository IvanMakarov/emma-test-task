
## Setup

1. Start project
```bash
$ git clone https://gitlab.com/IvanMakarov/emma-test-task.git
$ cd emma-test-task
$ cp ./example.env ./.env 
$ docker-compose up
```
2. Access the services at the following ports (adjustable at your own discretion in `docker-compose.yml`):
  - `localhost:8080` OpenAPI GUI
  - `localhost:8081` Mongo Express 
  - `localhost:3000` API server 

3. Before you can start claiming your rewards:
    1. Create a user with **POST** `/v1/register`
    2. Log in with your credentials with **POST** `/v1/login`
    3. Use the token string in the response to authorize your subsequent queries, e.g. `/v1/claim-free-share`, by copying and pasting it to the **Authorize** dialog in the OpenAPI GUI or the `authorization` header of your requests if using a different tool

4. Run tests
```bash
$ cd server
$ yarn 
$ yarn test
```

## Assumptions
- There is a check for user eligibility to a reward but the eligibility value is never updated and the claim endpoint is not throttled in any way
- It is not clear why `Broker.getRewardsAccountPositions` should return a single object, so it returns an array in this implementation
- _Emma wants to reward new users with **a** free share_ implies there is going to be only *one* share per reward
- `Broker` API does not have a method to query assets by price range and its implementation is out of scope for this project; I just go through all available assets randomly in a most straightforward manner to find the first one that fits the reward tier.
- The requirements do not require to implement any authentication but I just happened to some handy boilerplate from a recent project so there it is

## Other notes
I chose Typescript, Mongo and Mocha for this project because they are _not_ on my daily tools list and I just wanted some extra practice with these.

The project was tested on Ubuntu `18.04.6 LTS`, Docker `v.20.10.7`, Docker Compose `v.1.24.0`.

[Requirements doc](https://emmafinance.notion.site/Backend-Engineering-Challenge-2022-c6bd492ac5e042a4b6a6698e8fed7a24)